package Links;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# Link Object, which contains two linked breakpoints
sub new {
	my ($class, $breakpoint_1, $breakpoint_2) = @_;
	$class = ref($class) || $class;
	my $link = {};
	bless($link, $class);
	# Breakpoints are linked to each other
	$breakpoint_1->linkedTo($breakpoint_2);
	# ... are stored, and are sorted for link comparisons
	@{ $link->{breakpoints} } = BreakPoints::sortPartnerBreakpoints($breakpoint_1, $breakpoint_2);
	# Breakpoints are both children to the link instance
	$breakpoint_1->{parent} = $link;
	$breakpoint_2->{parent} = $link;
	# And their declaration order in the link is kept in memory
	$link->{breakpoints}[0]->{order} = 0;
	$link->{breakpoints}[1]->{order} = 1;
	return($link);
}

# UTILISE
sub updateReciprocalEventOccurrences {
	my ($link, $compared_link, $option_hash) = @_;
	$link->updateEventOccurrences($compared_link, $option_hash);
	$compared_link->updateEventOccurrences($link, $option_hash);
}

# UTILISE
sub updateReciprocalLinkOccurrences {
	my ($link, $compared_link, $option_hash) = @_;
	$link->updateLinkOccurrences($compared_link, $option_hash);
	$compared_link->updateLinkOccurrences($link, $option_hash);
}

# UTILISE
sub updateEventOccurrences {
	my ($link, $compared_link, $option_hash) = @_;
	# If no events are identical to this event in the compared sample, the occurrences increase
	if (!($link->{samples}{ $compared_link -> {sample} }{identical_events}[0])) {
		$link->{occurrences}++;
	}
	# We store the breakpoint
	push @{ $link->{samples}{ $compared_link -> {sample} }{identical_events} }, $compared_link;
}


# UTILISE
sub updateLinkOccurrences {
	my ($link, $compared_link, $option_hash) = @_;
	# If no events present the same breakpoint association that this event in the compared sample, the occurrences increase
	if (!(defined($link->{samples}{ $compared_link -> {sample} }{linked_events}[0]))) {
		$link->{link_occurrences}++;
	}
	# We store the breakpoint
	push @{ $link->{samples}{ $compared_link -> {sample} }{linked_events} }, $compared_link;
}

# UTILISE
sub getComplexEvent {
	my ($link, $option_hash) = @_;
	#  The computation is assumed to be done once
	if (!(defined($link -> {complex_event}))){
		my %events;
		$link->getComplexEvents(\%events);
		if (keys %events > 1) {
			$$option_hash{"complex_events"}{"current_number"}++;
			foreach my $link_from_complex (keys %events) {
				$events{$link_from_complex}->{complex_event} = $$option_hash{"complex_events"}{"current_number"};
			}
		# If there is only one event, it is obligatory the current one
		} else {
			$link->{complex_event} = 0;
		}
	}
	return($link->{complex_event});
}

# UTILISE
# Recursive function which adds events to a temporary hash until all linked events have already been seen
sub getComplexEvents {
	my ($link, $link_temp_hash) = @_;
	$$link_temp_hash{$link} = $link;
	foreach my $call (keys %{ $link -> {calls} }) {
		foreach my $compared_call (keys %{ $link -> {calls}{$call}->{linkedTo} }) {
			my $compared_event = $link -> {calls}{$call}->{linkedTo}{$compared_call}->{parent};
			if (defined($compared_event) && !($$link_temp_hash{$compared_event})) {
				$compared_event->getComplexEvents($link_temp_hash);
			}
		}
	}
}

# UTILISE
sub computeLength {
	my ($link) = @_;
	if (!(defined($link->{length}))) {
		if  ( $link->{breakpoints}[0]->{chromosome} ne $link->{breakpoints}[1]->{chromosome} ) {
			$link->{length} = "NA";
		} else {
			$link->{length} = $link->{breakpoints}[1]->{end} - $link->{breakpoints}[0]->{start} + 1;
		}
	}
	return $link->{length};
}



1;