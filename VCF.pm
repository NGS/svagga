package VCF;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => "all";

use Switch;

our %basicFieldTypes = (
	"fileformat" => 1,
	"fileDate" => 1,
	"source" => 1,
	"cmdline" => 1,
	"reference" => 1,
);

our %containingIDFields = (
	"contig" => 1,
	"INFO" => {
		"tested_fields" => {
			"Number" => {
				"authorized_values" => {
					"1" => 1,
					"0" => 1,
					"." => 1,
					"2" => 1,
				},
			}
		},
	},
	"ALT" => 1,
	"FILTER" => 1,
	"FORMAT" => 1,
	"SAMPLE" => 1,
);

our %mandatoryColomns = (
	"#CHROM" => 1,
	"POS" => 1,
	"ID" => 1,
	"REF" => 1,
	"ALT" => 1,
	"QUAL" => 1,
	"FILTER" => 1,
	"INFO" => 1,
	"FORMAT" => 1,
);

# UTILISE
sub addHeaderInformationFromLine {
	my ($header_hash, $line) = @_;
	$line = Files::megaChomp($line);
	if ($line =~ s/^##//) {
		$line =~ m/^([^\=]+)=(.+)/;
		my $fieldType = $1;
		my $info = $2;
		switch ($fieldType) {
			# Basic type, "field=info" format
			case %basicFieldTypes {
				$$header_hash{$fieldType} = $info;
			}
			case %containingIDFields {
				addContainingIDFieldInfo($header_hash, $fieldType, $info);
			} else {
				print $line."\n";
			}
		}
	} else {
		# The #CHROM [...] line is saved in a header "fields"
		if ($line =~m/^#/) {
			@{ $$header_hash{fields} } =  split("\t", $line);
			if ($$header_hash{fields}[9]) {
				@{ $$header_hash{samples} } = @{ $$header_hash{fields} }[9 .. $#{ $$header_hash{fields} }];
			} else {
				die("No sample found in the VCF :\n$line.\n");
			}
			return(0);
		} else {
			die Errors::ProgrammerBug("Impossible to parse header line : \n$line.\n")."\n";
		}
	}
	return(1);
}


# UTILISE
sub addContainingIDFieldInfo {
	my ($header_hash, $fieldType, $info) = @_;
	$info =~ s/^\<(.+)\>$/$1/;
	my @Infos = split(",", $info);
	my %temp_hash;
	my $id;
	foreach my $i (@Infos) {
		my @I = split("=", $i);
		# The ID will be the key of the hash
		if ($I[0]  eq "ID") {
			$id = $I[1];
		}
		$temp_hash{$I[0]} = $I[1];
		
		# Specific cases
		# INFO
		if ($fieldType eq "INFO") {
			# Some values have to be tested, because all are not taking into account
			if ($containingIDFields{"INFO"}{"tested_fields"}{$I[0]} && !($containingIDFields{"INFO"}{"tested_fields"}{$I[0]}{"authorized_values"}{$I[1]})) {
				die "Bad ##INFO field in the VCF file :\nWe apologize, but the value ".$I[1]." of the field ".$I[0]." is not taking into account for the moment.\n"; 
			}
		}
	}
	#The temporary hash is assignated to the ID reference (example : "chr1") of the fieldType reference (example : "contig")
	if (defined($id)) {
		%{ $$header_hash{lc($fieldType)}{$id} } = %temp_hash;
	} else {
		die "VCF error - or programmer bad VCF parsing ? : the $fieldType field type is supposed to have an ID key.\n";
	}
}

# UTILISE
sub testFileInstanceReference {
	my ($file_instance) = @_;
	foreach my $chromosome (keys %{ $file_instance->{header}->{contig} }) {
		if($Genomes::authorized_chromosomes{$chromosome}) {
			if ($file_instance->{header}->{contig}{$chromosome}{length} && $file_instance->{header}->{contig}{$chromosome}{length} != $Genomes::authorized_chromosomes{$chromosome}{length}) {
				die(Errors::badLengthChromosome($chromosome, $file_instance->{header}->{contig}{$chromosome}{length}, $Genomes::authorized_chromosomes{$chromosome}{length}));
			}
		} else {
			die(Errors::unknownChromosome($chromosome));
		}
	}
}

# UTILISE
sub parseVariantLine {
	my ($line, $header_hash) = @_;
	my @elements = Files::megaSplit($line);
	my %vcf_information;
	my $error_detected;
	#Foreach element of the variant line
	foreach my $element_number (0 .. $#elements) {
		my $element = $elements[$element_number];
		my $field = $$header_hash{fields}[$element_number];
		# If there is a corresponding header field, treat it
		switch ($field) {
			case "#CHROM" {
				$vcf_information{chrom} = $element;
			}
			case "POS" {
				$vcf_information{pos} = $element;
			}
			case "ID" {
				$vcf_information{id} = $element;
			}
			case "REF" {
				$vcf_information{ref} = $element;
			}
			case "ALT" {
				if ($element =~ m/,/) {
					die "Not possible yet to have several ALT...\n";
				}
				my @ALT = split(",", $element);
				$vcf_information{alt} = $ALT[0];
			}
			case "QUAL" {
				$vcf_information{qual} = $element;
			}
			case "FILTER" {
				$vcf_information{filter} = $element;
			}
			case "INFO" {
				%{ $vcf_information{info} } = parseINFOField($element, $header_hash, $line);
			}
			case "FORMAT" {
				# %{ $vcf_information{format} } = parseFORMATField($element);
			} else {
				# It should be a sample
				if (defined($field)) {
					foreach my $sample (@{ $$header_hash{samples} }) {
						# $vcf_information{samples}{$sample} = add...
					}
				# Or maybe, the line is malformated?
				} else {
					die "It seems there is more elements in the following header :\n$line\n";
				}
			}
		}
	}
	return(%vcf_information);
}

# UTILISE
sub parseINFOField {
	my ($info_fields, $header_hash, $line) = @_;
	$info_fields = Files::megaChomp($info_fields);
	my %info_hash;
	my @fields = split (";", $info_fields);
	foreach my $field_and_values (@fields) {
		my ($field, $values) = split ("\=", $field_and_values);
		# We test the value coherence with the field declaration
		my $error = testValueConsistency($values, $$header_hash{info}{$field});
		if ($error) {
			print $error."\n";
			die Errors::malformedVCFInfoLine($line);
		}
		# If there is no value, the $field is mandatory a "Flag", and $values become 1
		if (!($values)) {
			$values = 1;
		}
		$info_hash{$field} = $values;
	}
	return(%info_hash);
}

# UTILISE
sub testValueConsistency {
	my ($values, $info_field) = @_;
	# Values
	my @values;
	my $error;
	# $values can be equal to 0
	if (defined($values)) {
		@values = split(",", $values);
	}
	# Test of the number of values
	$error = testValueNumber(\@values, $$info_field{Number});
	if ($error) {
		return $error;
	}

	# Test of the value itself
	foreach my $value (@values) {
		$error = testValueType($value, $$info_field{Type});
		if ($error) {
			return $error;
		}
	}
	return 0;
}

# UTILISE
sub testValueNumber {
	my ($values, $number) = @_;
	my $message = "Wrong number ($number) of elements for the table :\n".join(",", @$values)."\n";
	switch ($number) {
		case /^[0-9]+$/ {
			# The number 0 means a Flag, with no value
			if ($number == 0) {
				if ($$values[0]) {
					return $message;
				}
			} else {
			# The other number imply the same number of values
				if (!(defined($$values[0])) || @$values != $number) {
					return $message;
				}
			}
		}
	}
	return 0;
}

# UTILISE
sub testValueType {
	my ($value, $type) = @_;
	my $message = "Bad $type type for the value $value.\n";
	switch ($type) {
		case 'Integer' {
			if ($value !~ m/^-*[0-9]+$/) {
				return 0;
			} 
		}
		case 'Float' {
			if ($value !~ m/^-*[0-9]+\.*0-9]*$/) {
				return 0;
			} 
		}
		case 'Character' {
			if ($value !~ m/^\S$/) {
				return 0;
			}
		}
	}
	return 0;
}

1;