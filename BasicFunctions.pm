package BasicFunctions;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings;

# Function which takes a variable x, and a value y, and return x if defined, y if x undefined
sub getIfDefined {
	my ($variable, $value) = @_;
	if (!(defined($variable))) {
		$variable = $value;
	}
	return($value);
}

# Function which takes an array and two positions, and move the element present at the old position until the new position
sub moveElement {
	my $array = $_[0];
	my $new_position = $_[1];
	my $old_position = $_[2];
	splice (@$array, $new_position, 0, splice(@$array, $old_position, 1) );
}

# Function which takes a character and return it x times in an array
sub repeatCharacter {
	my $character = $_[0];
	my $repeats = $_[1];
	my @characters;
	for my $i (1 .. $repeats) {
		push @characters, $character;
	}
	return(@characters);
}



1;
