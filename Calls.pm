package Calls;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;

# A Call is a Link with a Caller, a Sample, and other information
our @ISA = ("Links");

our @indispensableFields = (
	"(Mate_)Chromosome",
	"(Mate_)Position/Start",
);

# Call object, which contains (for the moment) two breakpoint objects, a type of event (deletion, duplication etc.), and other information
sub new {
	my ($class, $breakpoint_1, $breakpoint_2, $event_type, $caller, $sample, $annotations) = @_;
	$class = ref($class) || $class;
	my $call = {};
	$call = Links->new($breakpoint_1, $breakpoint_2);
	# Other information
	$call->{"event_type"} = $event_type;
	$call->{"sample"} = $sample ? $sample : "Unknown";
	$call->{"caller"} = $caller ? $caller : "Unknown";
	$call->{"annotations"} = $annotations ; # Hash ref
	$call->{"source_file"} = undef ;
	bless($call, $class);
	return($call);
}

# UTILISE
sub testChromosome {
	my ($call) = @_;
	foreach my $breakpoint (@{ $call->{breakpoints} }) {
		Genomes::dieIfNonexistentChromosome($breakpoint->{chromosome});
	}
}

# UTILISE
sub getCallsFromFiles {

	my ($option_hash, $data_hash) = @_;
	# TODO 
	# Frequent call/event file : will allow to quickly skip recurrent calls.
	if (defined(@{ $$option_hash{files}{excluded_call_files} }[0])) {
		print "## Acquisition of the excluded Calls ##\n";
		foreach my $call_file (@{ $$option_hash{files}{excluded_call_files} }) {
		 	print "- File : ".$call_file."\n";
			my $call_file_instance = Calls::createAndStoreFile($call_file, $option_hash, $data_hash);
			$call_file_instance -> {precise_content} = "excluded_calls";
		 	getCallsFromFile($call_file_instance, $option_hash, $data_hash);
		}
		print "\n";
	}

	# Reference call files
	if (defined(@{ $$option_hash{files}{reference_call_files} }[0])) {
		print "## Acquisition of the Reference Calls/Events ##\n";
		foreach my $call_file (@{ $$option_hash{files}{reference_call_files} }) {
		 	print "- File : ".$call_file."\n";
			my $call_file_instance = Calls::createAndStoreFile($call_file, $option_hash, $data_hash);
			$call_file_instance -> {precise_content} = "reference_calls";
		 	getCallsFromFile($call_file_instance, $option_hash, $data_hash);
		}
		print "\n";
	}
	# Other call files
	if (defined(@{ $$option_hash{files}{call_files} }[0])) {
		print "## Acquisition of the Calls ##\n";
		foreach my $call_file (@{ $$option_hash{files}{call_files} }) {
		 	print "- File : ".$call_file."\n";
			my $call_file_instance = Calls::createAndStoreFile($call_file, $option_hash, $data_hash);
			$call_file_instance -> {precise_content} = "calls";
		 	getCallsFromFile($call_file_instance, $option_hash, $data_hash);
		}
		print "\n";
	}
	# Now, the targets are calls and no more simple breakpoints
	$$option_hash{"impression"}{"targetted_entities"}[0] = "calls";
}

# UTILISE
sub createAndStoreFile {
	my ($call_file, $option_hash, $data_hash) = @_;
	my $call_file_instance = Files::createAndStoreFile($call_file, $data_hash);
	$call_file_instance -> {content_type} = "calls";
	if ($$option_hash{"inputs"}{"interprete_name"}) {
		$call_file_instance->interpreteName();
	}
	return ($call_file_instance);
}


# UTILISE
sub getCallsFromFile {
	my ($call_file_instance, $option_hash, $data_hash) = @_;
	my @calls;

	my @formats = ("VCF", "TAB");
	if (!($call_file_instance->{format} eq "VCF" || $call_file_instance->{format} eq "TAB")) {
		die(Errors::badFormatForFileAmongPossibilities($call_file_instance->{name}, "call files", $call_file_instance->{format}, \@formats));
	}

	# Sample and caller can be interpreted from the file name
	# Files::interpetSampleAndCaller($call_file_instance, $option_hash);

	# If the file is a reference, keep it in mind, it will not be printed
	if ($call_file_instance->{precise_content} =~ m/reference/ || $call_file_instance->{precise_content} =~ /excluded/) {
		$call_file_instance->{not_to_print} = 1;
	}
	# Extraction of the header information
	$call_file_instance->treatHeader();
	if (!(isValidHeader($call_file_instance))) {
		die Errors::callFileInvalidHeader($call_file_instance->{name});
	}
	# We eventually test that the call file and the reference genome file present the same contigs
	$call_file_instance->testReference();

	# Variables
	my %temp_hash;
	my %seen_callers;
	# Sample (for the moment, we treat only one sample per file)
	my $sample = "Unknown";
	if ($call_file_instance->{sample}) {
		$sample = $call_file_instance->{sample};
	} elsif ($call_file_instance->{"format"} eq "VCF") {
		$sample = $call_file_instance->{header}{samples}[0];
	}
	# TAF : declarer les callers identifiés
	my $caller = "Unknown";
	if ($call_file_instance->{caller}) {
		$caller = $call_file_instance->{caller};
	}
	$$option_hash{variant_callers}{used}{ $caller } = 1;
	$seen_callers{$caller} = 1;

	# Reading of the calls
	CALLS: while (my $line = readline($call_file_instance->{"file_handler"})) {
		$line = Files::megaChomp($line);
		if($line =~ /^\s*$/) {
			next CALLS;
		}
		# We get the information which will allow to create the breakpoints and identify/create the call
		my %info = getInfoFromLine($line, $call_file_instance, $option_hash, $data_hash);
		# We notice that the sample has been seen in inputs
		my $sample_instance = Samples::findOrNew($sample, $data_hash);
		$sample_instance -> setSeen();
		# Event interpretation
		my $event_type = Events::interpretedEventFromInfoLine(\%info, $call_file_instance->{"format"}, $option_hash);
		if ($info{id} && $temp_hash{ $info{id} }) {
			die(Errors::alreadySeenID($call_file_instance, $info{id}, $line)); # An ID can only be seen once
		}
		# We create the breakpoints and the parent call from the info fields
		my ($target_breakpoint, $mate_breakpoint) = BreakPoints::createBreakpointCoupleFromInfoLine(\%info, $call_file_instance->{"format"}, $option_hash);
		my 	$call = Calls->new($target_breakpoint, $mate_breakpoint, $event_type, $caller, $sample);
		if ($$option_hash{events}{interpretation} eq "1") {
			$call->{event_type} = Events::testEventTypeConsistency($event_type, $target_breakpoint->{chromosome}, $mate_breakpoint->{chromosome});
		}
		my ($existing_target_breakpoint, $existing_mate_breakpoint, $found) = BreakPoints::foundBreakPointCoupleInHash($target_breakpoint, $mate_breakpoint, $call, \%temp_hash);
		# If the two breapoints already exist, the only thing to do is get the annotation specifict to the targetted breakpoint
		if ($found) {
			%{ $existing_target_breakpoint->{annotations} } = %{ $target_breakpoint->{annotations} };
			$target_breakpoint = $existing_target_breakpoint;
			$mate_breakpoint = $existing_mate_breakpoint;
			# $call = $target_breakpoint->{parent}; # Not used, but I prefer write it if it is used one day
		} else {
			
			$target_breakpoint->storeBreakpointInHash($call, \%temp_hash); # We only store the targe to say we meet it (we did not meet the mate)
			$call->{source_file} = $call_file_instance;
			$call->testChromosome();
			foreach my $breakpoint (@{ $call->{breakpoints} }) {
				my $chromosome = $breakpoint->{"chromosome"};
				$breakpoint->storeAndSortOnChromosomeArray(\@{ $$data_hash{samples}{  $call->{"sample"} }{ $call_file_instance -> {content_type} }{chromosomes}{ $breakpoint->{"chromosome"} } });		
			}
		}
		if (!($mate_breakpoint)) {
			print ("Impossible to detect the mate breakpoint in the following line :\n$line"."Maybe there is a problem of format.\nPlease notice that the single breakends (see VCF specifications) are not taking into account.\n");
			next CALLS;
		}
		push @{ $$data_hash{"files"}{"calls"}{ $call_file_instance->{name} } }, $target_breakpoint; #In the file, the calls are saved following the called breakpoints
	}
	
	# %temp_hash was created for commodity, but now the seen breakpoints are definitvely stored in data
	# No purpose for instance ?
	# %{ $$data_hash{"files"}{ $call_file_instance -> {name} }{"breakpoints"}{"known"} } = %temp_hash;

	# Storage of the caller-specific fields
	my %info;
	if ($call_file_instance->{"format"} eq "VCF") {
		%info = %{ $call_file_instance->{header}{info} };
	} else {
		%info = %{ $call_file_instance->{header}{fields} };
	}
	foreach my $caller (%seen_callers) {
		foreach my $field_name (keys %info) {
			my $field = $info{$field_name};
			if (!(defined($field->{"source_type"}{"basicTrack"}))) {
				# The special fields are also attributed to the caller
				$$data_hash{variant_callers}{$caller} -> {fields}{$field_name} = 1;
				# And vice versa
				$$data_hash{fields}{$field_name}{callers}{$caller}  = 1;
			}
		}
	}
}

# UTILISE
sub isValidHeader {
	my ($call_file) = @_;

	if ($call_file->{format} eq "TAB") {
		# Only chromosomes are mandatory
		if (!($call_file->{header}{fields}{Chromosome} && $call_file->{header}{fields}{Mate_Chromosome})) {
			return 0;
		}
		# For each partner, the start or position is mandatory too
		if (!($call_file->{header}{fields}{Start} || $call_file->{header}{fields}{Position})) {
			return 0;
		}
		if (!($call_file->{header}{fields}{Mate_Start} || $call_file->{header}{fields}{Mate_Position})) {
			return 0;
		}
	} elsif ($call_file->{format} eq "VCF") {
	}

	return 1;
}



# UTLISE
sub getInfoFromLine {
	my ($line, $call_file_instance, $option_hash, $data_hash) = @_;
	my %info;
	if ($call_file_instance->{format} eq "VCF") {
		%info = VCF::parseVariantLine($line, \%{ $call_file_instance->{header} });
	} else {
		%info = getACallFromATABLine($line, $call_file_instance, ,$option_hash, $data_hash);
	}
	return(%info);
}


# UTILISE
sub getACallFromATABLine {
	my ($line, $call_file, ,$option_hash, $data_hash) = @_;
	my %info;

	my @raw_information = Files::megaSplit($line);
	# For each field in the file, we get the corresponding value
	foreach my $field_number (0 .. $#{ $call_file->{header}{raw_fields} }) {
		my $field = $call_file->{header}{raw_fields}[$field_number];
		if (defined($raw_information[ $field->{position} ])) {
			$info{ $field->{precise_interpreted_track} } = $raw_information[ $field->{position} ];
		} else {
			die Errors::missingColumnInLine($call_file->{base_name}, $field->{position}, $line);
 		}
	}
	return (%info);
}

# UTILISE
# Function which returns the call on the form "chr1:1111-2222;chr2:1111-22222;event;sample;caller"
sub flatName {
	my ($call) = @_;
	return $call->{breakpoints}[0]->toSimpleBPString().";".$call->{breakpoints}[1]->toSimpleBPString().";".$call->{sample}.";".$call->{event_type}.";".$call->{caller}."\n";
}

# UTILISE
# Function which aggregates call sorted on the chromosomes by their breakpoints
sub aggregateCallsBySample {
	my ($option_hash, $data_hash) = @_;
	print "## Call aggregation ##\n";
	# We declare that breakpoints have been clustered for the ongoing step
	$$option_hash{breakpoints}{clustered} = 1;
	# Call information storage
	foreach my $sample (@{ $$data_hash{"sorted_samples"}{batch} }) {
		if ($$data_hash{objects}{samples}{$sample}->{target}) {
			print "- ".$sample."\n";
			$$data_hash{objects}{samples}{$sample} ->{aggregated} = 1;
			aggregateASampleCalls($sample, $option_hash, $data_hash);
		}
	}
	# Now, the targets are aggregated_calls and no more calls nor breakpoints
	$$option_hash{"impression"}{"targetted_entities"}[0] = "aggregated_calls";
	print "\n";
}

sub aggregateASampleCalls {
	my ($sample, $option_hash, $data_hash) = @_;
	CHROMOSOME: foreach my $chromosome (@Genomes::ordered_chromosomes) {
		if (!(defined(${ $$data_hash{samples}{$sample}{calls}{chromosomes}{$chromosome} }[0]))) {
			next CHROMOSOME;
		}
		BP: foreach my $breakpoint_nb (0 .. $#{ $$data_hash{samples}{$sample}{calls}{chromosomes}{$chromosome} } ) {
			my $breakpoint = ${ $$data_hash{samples}{$sample}{calls}{chromosomes}{$chromosome} }[$breakpoint_nb];
			my $call = $breakpoint->{parent};

			# For each sorted breakpoint of the chromosome (empty at the beginning of the loop)
			COMP: foreach my $compared_breakpoint_nb (($breakpoint_nb +1) .. $#{ $$data_hash{samples}{$sample}{calls}{chromosomes}{$chromosome} } ) {
				my $compared_breakpoint = ${ $$data_hash{samples}{$sample}{calls}{chromosomes}{$chromosome} }[$compared_breakpoint_nb];
				my $compared_call = $compared_breakpoint->{parent};

				# We do not consider the BP from the same Call
				if ($compared_call eq $call) {
					next COMP;
				}

				# If the next compared breakpoint (COMP) is too far, we can go to the next BP
				if (($breakpoint->{"end"} + $$option_hash{"breakpoints"}{"distance"}) < $compared_breakpoint->{"start"}) {
					# If we were looking at the last breakpoint of the call (order == 1)
					# .. and if the call cannot be merged with any of the other call
					# => It becomes itself a new event
					if($breakpoint->{order} == 1 && !(defined($call->{parent}))) {
						my $event = new Events();
						$event->addCallToEvent($call);
						foreach my $breakpoint (@{ $event->{breakpoints} }) {
							$breakpoint->storeAndSortOnChromosomeArray(\@{ $$data_hash{samples}{  $event->{"sample"} }{aggregated_calls}{chromosomes}{ $breakpoint->{"chromosome"} } });
						}
					}
					next BP;
				}

				# If the two breakpoint overlap, they are linked
				if ($breakpoint->overlapWithDistance($compared_breakpoint, $$option_hash{"breakpoints"}{"distance"})) {
					$breakpoint->setReciprocalOverlap($compared_breakpoint);
					# Putatively, the clustered breakpoints lead to clustered calls
					# Because breakpoints are sorted, they can lead to a call fusion uniquely if both are the last of their respective call
					if($breakpoint->{order} == 1 && $compared_breakpoint->{order} == 1 && $breakpoint->{mate}->{overlapWith}{ $compared_breakpoint->{mate} }) {
						if ($call->comparingEvents($compared_call, $option_hash)) {
							$call->aggregateWith($compared_call, $option_hash, $data_hash);
						}
					}
				}
			}

			# At the end of the loop, if the parent call of the last breakpoint is not aggregated
			# We have to transform the call into an event and store it
			if ($breakpoint->{order} ==1 && !(defined($call->{parent}))) {
				my $event = new Events();
				$event->addCallToEvent($call);
				foreach my $breakpoint (@{ $event->{breakpoints} }) {
					$breakpoint->storeAndSortOnChromosomeArray(\@{ $$data_hash{samples}{  $event->{"sample"} }{aggregated_calls}{chromosomes}{ $breakpoint->{"chromosome"} } });
				}
			}

		}
	}
}

# UTILISE
# Function which estimates, based on their respective events and options, if two calls are "mergeable"
sub comparingEvents {
	my ($call, $compared_call, $option_hash) = @_;
	# Some options totally evade the notion of events
	if ($call->{event_type} eq $compared_call->{event_type}) {
		# Breakend are merged only if they have the same orientation
		if ($call->{event_type} eq "Breakend") {
			# but we can choose not to deal with BND orientation
			if($$option_hash{events}{ignore_bnd_orientation} || $call->haveSameOrientation($compared_call)) {
				return 1;
			} else {
				return 0;
			}
		# Other events are merged if they are of same type
		} else {
			return 1;
		}
	} else {
		if ($$option_hash{events}{ignore_event_type}) {
			return 1;
		} else {
			if ($$option_hash{events}{merge_bnd}) {
				if ($call->{event_type} eq "Breakend" || $compared_call->{event_type} eq "Breakend") {
					return 1;
				} else {
					return 0;
				}
			} else {
				return 0;
			}
		}
	}
	die();
}



# UTILISE
# Function which aggregates two calls into an aggregated_call, and manage the existing aggregated_calls on a data array
sub aggregateWith {
	my ($call, $toBeMergedCall, $option_hash, $data_hash) = @_;
	my @calls = ($call, $toBeMergedCall);
	# In the case both calls are still belonging to an event,
	# the options will determine how to deal with these aggregated_calls
	if (defined($call->{parent}) && defined($toBeMergedCall->{parent})) {
		if ($call->{parent} eq $toBeMergedCall->{parent}) { # If A,B,C merging and in this order, B merges with A, C merges with A, and C then C merges with B (already merged)
			return;
		} else {
			# print "Calls : ".$call->toSimpleFlatStringWithOrientation()."\t".$toBeMergedCall->toSimpleFlatStringWithOrientation()."\n\n";
			# print "Events".$call->{parent}->toSimpleEventStringWithCallsWithOrientation()."\n\n".$toBeMergedCall->{parent}->toSimpleEventStringWithCallsWithOrientation()."\n\n";
			return;
		}
	} elsif (defined($call->{parent})) {
		$call->{parent}->addCallToEvent($toBeMergedCall, $option_hash);
	} elsif (defined($toBeMergedCall->{parent})) {
		$toBeMergedCall->{parent}->addCallToEvent($call, $option_hash);
	} else {
		my $event = new Events(\@calls);
		foreach my $breakpoint (@{ $event->{breakpoints} }) {
			$breakpoint->storeAndSortOnChromosomeArray(\@{ $$data_hash{samples}{  $event->{"sample"} }{aggregated_calls}{chromosomes}{ $breakpoint->{"chromosome"} } });
		}
	}
}

# UTILISE
sub toSimpleCallString {
	my ($call) = @_;
	my $string = $call->{event_type}."\n";	
	foreach my $breakpoint (@{ $call->{breakpoints} }) {
		$string .= "breakpoint ".$breakpoint->{order}.":".$breakpoint->toSimpleBPString()."\n";
	}
	return($string);
}

sub toSimpleFlatString {
	my ($call) = @_;
	return $call->{event_type}."=".$call->{breakpoints}[0]->toSimpleBPString().";".$call->{breakpoints}[1]->toSimpleBPString();
}

sub toSimpleFlatStringWithOrientation {
	my ($call) = @_;
	return $call->{event_type}."=".$call->{breakpoints}[0]->toSimpleBPStringWithOrientation().";".$call->{breakpoints}[1]->toSimpleBPStringWithOrientation();
}

# Function which tests that the call breakpoints cluster two by two
sub overlap {
	my ($call, $compared_call) = @_;
	#If at least one pair does not cluster, the calls are not clustered
	foreach my $breakpoint_nb (0 .. $#{ $call->{breakpoints} }) {
		if (!($call->{breakpoints}[$breakpoint_nb]->{overlapWith}{ $compared_call->{breakpoints}[$breakpoint_nb] })) {
			return(0);
		}
	}
	return(1);
}

# UTILISE
# Function which tests that the call breakpoints have same orientations two by two
sub haveSameOrientation {
	my ($call, $compared_call) = @_;
	#If at least one pair does not cluster, the calls are not clustered
	foreach my $breakpoint_nb (0 .. $#{ $call->{breakpoints} }) {
		my ($orientation, $compared_orientation) = ($call->{breakpoints}[$breakpoint_nb]->{orientation}, $compared_call->{breakpoints}[$breakpoint_nb]->{orientation});
		if (defined($orientation) && defined($compared_orientation) && !($orientation eq $compared_orientation)) {
			return(0);
		}
	}
	return(1);
}

# OBSOLETE (dans la librairie Links)
sub updateOccurrences {
	my ($call, $compared_call, $option_hash) = @_;
	# If no calls are identical to this call in the compared sample, the occurrences increase
	if (!($call->{samples}{ $compared_call -> {sample} }{identical_calls}[0])) {
		$call->{occurrences}++;
	}
	# We store the breakpoint
	push @{ $call->{samples}{ $compared_call -> {sample} }{identical_calls} }, $compared_call;
}

# UTILISE
# If the target is a call, it has no other chlidren than itself
sub getCallChildren {
	return @_;
}

# # OBSOLETE
# # Function which is simply used not to repeat all the instructions for the two SV call positions...
# sub checkPositionsInHeader {
# 	my ($call_file, $field_hash, $position) = @_;

# 	# Variables
# 	my $p;
# 	my $start_but_not_end;

# 	if ($position == 1) {
# 		$p = "";	
# 	} elsif ($position == 2){
# 		$p = "_2";
# 	} else {
# 		die("Programming error!\n");
# 	}

# 	# At least Position or Start have to be precised
# 	if(defined($$field_hash{"Position".$p})) {
# 		# But not both
# 		if(defined($$field_hash{"Start".$p})) {
# 			die "The $call_file file header has two mutually exclusive fields : Start$p and Position$p. Please keep only one of them...\n";
# 		} else {
# 			$$field_hash{"Start".$p} = $$field_hash{"Position".$p};
# 		}
# 	} else {
# 		if(!(defined($$field_hash{"Start".$p}))) {
# 			die Errors::indispensableField($call_file, "call file","Position$p (which can be replaced with Start$p/End$p)");
# 		}  else {
# 			$start_but_not_end = 1;
# 		}
# 	}
# 	# End is deduced from Start if no End is furnished
# 	if (!(defined($$field_hash{"End".$p}))) {
# 		# But this geneates (at least) a warning
# 		# TODO : can die according to the selected options
# 		if ($start_but_not_end) {
# 			print "### WARNING ###\n";
# 			print "File $call_file : Start$p furnished, but no End$p.\n";
# 		}
# 		$$field_hash{"End".$p} = $$field_hash{"Start".$p};
# 	}
# }

1;

