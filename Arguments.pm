package Arguments;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => "all";

our @options_and_arguments = (
##### INPUTS ########
	#####################
	"version|v",
	# INPUTS
	"interpret_input_file_names|ifn=s",
	# Samples
	"target_samples|tsa=s@",
	# Caller
	# "variant_caller_configuration_file|caller_conf|variant_caller_information_file|caller_info=s@",
	# Genome
	"reference_contigs|contigs=s",
	# Genes
	# "omim_files|omim_file|omim=s@",
	# "gene_files|gene_databases|gene_db|genes|gene=s@",
	# "gene_annotation_files|gene_annot|gene_annotation|gene_annotation_file|gaf=s@",
	# Other information
	# "annotation_file_configuration|afc|annotation_cconfiguration|ann_conf=s@",
	# Breakpoints
	# "breakpoint_files|breakpoint_file|breakpoints|bp_file|bp_files|breakpoint_list|bpf=s@", #
	"maximum_breakpoint_distance|mbd=i", # SV overlap is the same notion that breakpoint distance, but more explicitly linked to SV
	# "breakpoint_context_length|genomic_context_lenght|gcl|context_bp|bp_context=i",
	# VCF parsing - Breakpoint extension
	"bnd_extension|bnd_ci=s",
	"bnd_extension_start|bnd_ci_start=s",
	"bnd_extension_end=s",
	# Calls
	"aggregate_calls|agg=s",
	"ignore_event_type|ignevt=s",
	"ignore_bp_orientation|io=s",
	"force_bnd_aggregation|fba=s",
	"call_files|calls=s@",
	# "reference_call_files|ref_sv|sv_ref|ref_call_file|ref_call_files|ref_sv_file=s@",
	# "excluded_call_files|excluded_calls|excluded_sv=s",
	# CNV specificity
	# "overlap|overlap_threshold|ot|cnv_overlap=s",
	# Events
	"interprete_events|intevt=s",
	# "event_maximum_occurrence|max_events|max_event|emo=i",
	# "event_maximum_frequency|freq_events|max_freq_events|emf=f",
	# "target_event_files|target_events|tef=s@",
	# Values
	# "round=i",
	# Value (from caller only for the moment) control
	# "value_control|vc=s",
	## OUTPUTS
	"output_directory|o=s",
	# Output files
	"output_suffix|suff=s",
	"existent_output_files|eof=s",
	"impression_logic|il=s",
	# Printed data
	"printed_field_configuration|print_fields|print_field|print_field_conf|pfc=s@",
	"print_only_first_breakpoint|pofb",
	# "no_partner_breakpoint_printing|npp",
	# Information format
	"show_all_ambiguous_events|show_all_events|unmask_events|show_ambiguous_events|saae"
);