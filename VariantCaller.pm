package VariantCaller;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>
use strict;
use warnings;

# Variables
our %known_methods = (
	"Read-depth" => {
		"shortcut" => "RD",
	},
	"Paired-ends" => {
		"shortcut" => "PE",
	},
	"Split-reads" => {
		"shortcut" => "SR",
	},
	"Assembly" => {
		"shortcut" => "AS",
	},
);


our %shorcuts = (
	"RD" => {
	},
	"PE" => {
	},
	"SR" => {
	},
	"AS" => {
	},
);


our $variant_caller_format = "variant caller configuration";
our @indispensable_fields = (
	"Caller",
	"Method",
);

# Object VariantCaller, which contains a name, a version, and is associated with methods
sub new {
	my ($class, $variant_caller, $version, $method, $separator) = @_;
	$class = ref($class) || $class;
	my $caller = {};
	bless($caller, $class);
	$caller->{"name"} = $variant_caller;
	$caller->{"version"} = $version;
	if (defined($method)) {
		%{ $caller->{"methods"} } = extractMethods($method);
	}
#	$caller->{"separator"} = $separator;
	%{ $caller->{"fields"} } = ();
	return($caller);
}

# UTILISE
sub extractMethods {
	my @method_list = split(",", $_[0]);
	my %methods;
	foreach my $method (@method_list) {
		$method = determineMethod($method);
		if($method) {
			$methods{$method} = 1;
		} else {
			die(Errors::unknownCallingMethod($method));
		}
	}
	return(%methods);
	
}

# UTILISE
# Object manipulation
sub addMethods {
	my ($variant_caller, $methods) = @_;
	my @methods = split(",", $methods);
	foreach my $method (@methods) {
		$method = determineMethod($method);
		if($method) {
			$variant_caller -> {"methods"}{$method} = 1;
		}
	}
}

#UTILISE 
sub determineMethod {
	my ($method) = @_;
	if ($known_methods{$method}) {
		return $known_methods{$method}{shorcut};
	} elsif ($shorcuts{$method}) {
		return $method;
	} else {
		return 0;
	}
}



# UTILISE
# Function which stores the information of a variant_caller configuration file to a hash
sub configureVariantCallers {
	my ($option_hash, $data_hash) = @_;

	print "## Configuration of the variant callers ##\n";
	foreach my $variant_caller_file (@{ $$option_hash{"raw_options"}{"variant_caller_configuration_file"} }) {
		print "- File : ".$variant_caller_file."\n";
		getVariantCallerInformationFromAFile($option_hash, $variant_caller_file, $data_hash);
	}
	print "\n";
}

# UTILISE
sub getVariantCallerInformationFromAFile {
	my ($option_hash, $variant_caller_file, $data_hash) = @_;
	open(my $variant_caller_conf_fh, "<", $variant_caller_file) or die(Errors::formatFileOpeningError($variant_caller_format, $variant_caller_file));
	my $header = <$variant_caller_conf_fh>;
	my $header_hash = Files::determinateHeaderFields($header, $variant_caller_file, 1); # Will store the header elements
	isCorrectHeader($variant_caller_file, $header_hash);
	# For each variant_caller, store information according to the header
	while (my $variant_caller_line = <$variant_caller_conf_fh>) {
		my $variant_caller_instance = transformVariantCallerLineIntoInstance($variant_caller_line, $header_hash);
		# The configured callers are indicated
		$$option_hash{variant_callers}{configured}{ lc($variant_caller_instance->{"name"}) } = 1;
		# The variant caller instance will be retrievable by name 
		$$data_hash{variant_callers}{ $variant_caller_instance->{"name"} } = $variant_caller_instance;
	}

}

# UTILISE
# Function which tests that there is no missing fields in the variant caller configuration file
sub isCorrectHeader {
	my ($variant_caller_file, $header_hash) = @_;
	foreach my $indispensable_field (@indispensable_fields) {
		if(!(defined($$header_hash{$indispensable_field}))) {
			die Errors::indispensableField($variant_caller_file, $variant_caller_format, $indispensable_field);
		}
	}
}

# UTILISE
# Function which get the information of a variant_caller from a line of the variant_caller configuration file
sub transformVariantCallerLineIntoInstance {
	my ($variant_caller_line, $header_hash) = @_;
	my @elements = Files::megaSplit($variant_caller_line);
	my $variant_caller_instance = VariantCaller->new();
	$variant_caller_instance -> {"name"} = $elements[$$header_hash{"Caller"}];
	$variant_caller_instance -> addMethods( $elements[$$header_hash{"Method"}] );
	return($variant_caller_instance);
}

# Function which reads information from the variant_caller configuration file, and returns it in order to create a VariantCaller instance
sub readVariantCallerLine {
	my $variant_caller_line = $_[0];
	chomp($variant_caller_line);
	my @VariantCaller_Infos = split("\t", $variant_caller_line);
	my @variant_caller_fields = ( @VariantCaller_Infos[0 .. 3], $VariantCaller_Infos[0]); # The values commune to every soft (colomns 0 to 3 - soft, version, method, separator) followed by the fields proper to the soft
	# TODO : store fields which are proper to the soft
	return(@variant_caller_fields);

}

# Function which takes the header of the variant_caller configuration file, and return the information (TODO: to really treat the header...)
sub headerTreatment {
	my $variant_caller_conf_fh = $_[0];
	my $header = <$variant_caller_conf_fh>;
	chomp($header);
	my @header_info = split("\t", $header);
	return(@header_info);
}

# Function which tests that the variant caller has been configured during the parametrisation step
sub testVariantCallerConfiguration {
	my $variant_caller = $_[0];
	my $data_hash = $_[1];

	if (!(defined($$data_hash{ $variant_caller }))) {
		Errors::noInformationForVariantCaller($variant_caller);
	}

}

# Pas encore utilisé
sub variantCallerControl {
	my ($option_hash, $data_hash) = @_;
	my @callers = keys %{ $$option_hash{variant_callers}{used} };
	$$option_hash{variant_callers}{total_used} = (@callers);
	my @Unknown_Callers;
	foreach my $caller (@callers) {
		if(!($$option_hash{variant_callers}{configured}{$caller})) {
			push @Unknown_Callers, $caller;
		}
	}
	if (defined($Unknown_Callers[0])) {
		my $message = "The following callers have been found in input files but were not declared in the configuration files :\n- ".join("\n- ", @Unknown_Callers)."\n";
		if (0) {
			print $message;
		} else { # TODO : the need of variant caller declaration depends on the desired outputs (for example, any metrics sorted by calling methods)
			die $message;
		}
	}
}

1;
