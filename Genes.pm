package Genes;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;

# A Gene is an Interval
use Intervals;
our @ISA = ("Intervals");

# Gene Object, an interval with a name, and other information such as transcripts
sub new {
	my ($class, $gene_name, $chromosome, $position_5p, $position_3p) = @_;
	$class = ref($class) || $class;
	my $gene = {};
	$gene = $class->Intervals::new( $chromosome, $position_5p, $position_3p );
	$gene->{"name"} = $gene_name;
	bless($gene, $class);
	return($gene);
}


sub getGenesFromFiles {
	my ($option_hash, $data_hash) = @_;
	print "## Acquisition of the Gene information ##\n";
	foreach my $gene_file (@{ $$option_hash{files}{gene_files} }) {
		print "# File : $gene_file\n";
		getGenesFromAFile($option_hash, $data_hash, $gene_file);
	}
	print "\n";
}

sub getGenesFromAFile {
	my ($option_hash, $data_hash, $gene_file) = @_;
	# Variables
	my $current_chromosome;
	my $taking_into_account;
	# File format
	my $format;
	if ($gene_file =~ m/\.gff[0-9]*$/) {
		$format = "GFF";
	} else {
		die "For the moment, only (non zipped) gff files are accepted for gene information file : $gene_file.\n"
	}
	# File storage
	my $file = new Files($gene_file, $format);
	$$data_hash{"files"}{"genes"}{$gene_file} = $file;
	# For each gene, the information is stored in the gene hash
	$file->openFile();
	LINE: while (my $line = readline( $file->{"file_handler"} )) {
		# Potential header # TODO : better?
		if ($format =~ "GFF" && $line =~ m/^\#/) {
			next LINE;
		}
		# Extractl ine information
		my $line = Files::megaChomp($line);
		my $separator = Files::determineSeparator($line);
		my @Info = split($separator, $line);
		my %Info = parseINFOField($Info[8]);
		# Feature-depending treatment
		switch ($Info[2]) {
			# If it is a genomic contig (chromosome), 
			case "region" {
				# For the moment, we take only the canonic chromosome
				# Do better : correspondance between the chromosome and the NC_
				if ($Info[0] !~ m/^NC_/) {
					$taking_into_account = 0;
					next LINE;
				} 
				# If we are on a canonical
				$taking_into_account = 1;
				if (defined($Info{"chromosome"})) {
					$current_chromosome = Genomes::dieIfNonexistentChromosome($Info{"chromosome"});
				# Work only with human genome...
				} elsif (defined($Info{"genome"}) && $Info{"genome"} eq "mitochondrion") {
					$current_chromosome = Genomes::dieIfNonexistentChromosome("chrM");
				} else { # Do better (link between NC_ and chromosomes in a file), but in the GFF we download we had this unique strange thing
						# NC_000007.13    Curated Genomic region  132767617       132767617       .       +       .       ID=id233132;Dbxref=GeneID:106479607,HGNC:HGNC:47055;gbkey=Region;gene=RNU6-92P
					print "None considered feature (gene or region ?) :\n".$line."\n";
					next LINE;
				}
				print "# Chromosome : $current_chromosome\n";
			}
			# TODO : use ID to link Genes and Transcripts
			# If it is a gene
			case "gene" {
				if (!($taking_into_account)) {
					next LINE;
				}
				# Instanciation
				my $gene_name = $Info{"Name"};
				if (!($gene_name)) {
					die("No gene \"name\" annotation :\n$line\n");
				}
				my $gene_instance = Genes->new($gene_name, $current_chromosome, $Info[3], $Info[4]);
				# Information
				if (defined($Info{Dbxref}) && $Info{Dbxref} =~ m/MIM\:([0-9]+)/) {
					$gene_instance->{omim}{id} = $1;
				}
				# Storage
				$gene_instance->storeAndSortOnChromosomeArray(\@{ $$data_hash{"genes"}{"chromosomes"}{$current_chromosome} }); # Sort by chromosome
				$$data_hash{"genes"}{"names"}{$gene_name}{$gene_instance} = $gene_instance; # Store gene by name (putatively non unique)
				$$data_hash{$gene_instance} = $gene_instance; # Shortcut to element

				# # Gene object
				# if (defined($$data{"genes"}{"id"}{ $infos{"ID"} })) {
				# 	die ("Non unique gene ID in the GFF33 file : ".$infos{"ID"}.".\n");
				# }

				# # Gene storage
				# $$data{"genes"}{"id"}{ $infos{"ID"} } = $gene; # Store gene by ID (unique)
			}
			
			# case %Transcripts::gff_transcripts {
			# 	my $transcript = Transcripts->new($infos{"transcript_id"}, $$data{"genes"}{"id"}{ $infos{"Parent"} }, $Chromosomes::chromosome_ids{$elements[0]}, $elements[3], $elements[4]);
			# 	if (defined($$data{"transcripts"}{"id"}{ $infos{"ID"} })) {
			# 		die ("Non unique gene ID in the GFF33 file : ".$infos{"ID"}.".\n");
			# 	}
			# 	$$data{"transcripts"}{"id"}{ $infos{"ID"} } = $transcript;
			# }
			# case "exon" {
			# 	my $exon = Exons->new($$data{"transcripts"}{"id"}{ $infos{"Parent"} }, $Chromosomes::chromosome_ids{$elements[0]}, $elements[3], $elements[4]);
			# }

		}

	}
}

# UTILISE
sub parseINFOField {
	my ($info_field) = @_;
	my %infos;
	my @fields = split (";", $info_field);
	foreach my $field (@fields) {
		my ($tag, $value) = split ("\=", $field);
		$infos{$tag} = $value; # We do no test here if value can be undefined
	}
	return(%infos);
}

sub getOMIMInformation {
	my $options = $_[0];
	my $data = $_[1];
	foreach my $omim_file (@{ $$options{files}{omim_files} }) {
		my $file = new Files($omim_file);
		$$data{"files"}{"omim"}{$omim_file} = $file;
		$file->openFile();
		my $line = readline($file->{"file_handler"}); # header
		while (my $line = readline( $file->{"file_handler"} )) {
			storeOMIMInfo($data, $line);
		}
	}
}

# OBSOLEETE
sub oldGetGenesFromFiles {
	my $options = $_[0];
	my $data = $_[1];
	foreach my $gene_file (@{ $$options{files}{gene_annotation_files} }) {
		my $file = new Files($gene_file);
		$$data{"files"}{"gene_annotations"}{$gene_file} = $file;
		$file->openFile();
		getGeneHeader($file);
		while (my $line = readline( $file->{"file_handler"} )) {
			storeGeneAnnotation($data, $file, $line);
		}
	}
}

sub storeOMIMInfo {
	my $data = $_[0];
	my $line = $_[1];
	chomp($line);
	my @elements = split("\t",$line);
	my $true_omim = 0;
	foreach my $element (@elements[1 .. $#elements]) {
		if ($element ne "0" && $element ne "NA") {
			$true_omim = 1;
		}
	}
	foreach my $gene (@{ $$data{"genes"}{"names"}{$elements[0]} }) { # Several genes can have the same name...
		$gene->{"is_omim"} = $true_omim;
	}
}

sub getGeneHeader {
	my $file = $_[0];
	my $line = readline($file->{"file_handler"});
	chomp($line);
	my @elements = split("\t", $line);
	# The header element are stored by order
	@{ $file->{"header_array"} } = @elements[1 .. $#elements];
	# The header element are associated to there number to retrieve corresponding info
	foreach my $element_nb (1 .. $#elements) {
		$file->{"header_hash"}{$elements[$element_nb]} = $element_nb - 1; 
	}
}

sub storeGeneAnnotation {
	my $data = $_[0];
	my $file = $_[1];
	my $line = $_[2];
	chomp($line);
	my @elements = split("\t",$line);
	foreach my $gene (@{ $$data{"genes"}{"names"}{$elements[0]} }) { # Several genes can have the same name...
		push @{ $gene->{"annotations"}{ $file->{"name"} } }, @elements[1 .. $#elements];
	}

}
sub annotateBreakPoints {
	my ($option_hash, $data_hash) = @_;
	print "## Crossing between Genes and Breakpoints ##\n";
	# Determination of the breakpoint location
	my $breakpoint_array = "calls";
	if ($$option_hash{breakpoints}{clustered}) {
		$breakpoint_array = "events";
	} elsif ($$option_hash{"task"} eq "breakpointAnnotation") {
		$breakpoint_array = "breakpoints";
	}
	SAMPLE: foreach my $sample (keys %{ $$data_hash{"samples"} }) {
		CHROMOSOME: foreach my $chromosome (@Genomes::ordered_chromosomes) {
			BREAKPOINT: foreach my $breakpoint (@{ $$data_hash{"samples"}{$sample}{$breakpoint_array}{"chromosomes"}{$chromosome} }) {
				ANNOTATION: foreach my $gene (@{ $$data_hash{"genes"}{"chromosomes"}{$chromosome} }){
					$breakpoint->annotateWithGene($gene);
				}
			}
		}
	}
	print "\n";
}

1;
