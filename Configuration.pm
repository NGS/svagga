package Configuration;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;

# OBSOLETE
# Main function which calls configuration subfuctions
sub configuration {
	my ($option_hash, $data_hash) = @_;

	# Gene database(s)
	Genes::getGeneInformation($option_hash, $data_hash);
	Genes::getOMIMInformation($option_hash, $data_hash); # A specific OMIM database
	Genes::getGeneAnnotations($option_hash, $data_hash); # Other gene databases

	# TODO : crossing between tracks (BED against BED, against Genes, etc.), in fuction of the desired fields
	# TODO : step determination based on the task and the options (return an array of steps to perform ?)
}

sub getConfigurationFileElements {
	my ($file) = @_;
	my $element_number = 0;
	my @elements;
	open (my $file_fh, "<", $file) or Errors::fileOpeningError($file);
	foreach my $line (<$file_fh>) {
		# Comments are authorized
		if($line =~ m/^\#/) {
			next;
		}
		my $temp_line = Files::megaChomp($line);
		# We store all the information on the pattern key=value, separated by spaces for a same track, and ";" between elements
		while ($temp_line =~ s/^\s*([^\s;]+)\=([^\s;]+)\s*//) {
			my $key = $1;
			if (defined($elements[$element_number]{$key})) {
				die "Element with duplicate key \"$key\" in file $file :\n$line";
			}
			$elements[$element_number]{$1} = $2;
			if ($temp_line =~ s/^;//) { # If after the space removal, we obtain a ";", it means that we have to increment the number of elements
				$element_number++;
			}
		}
		# If at the end of the process the line still possess information, there is a problem in the format
		if ($temp_line ne "") {
			die ("Problem in the file $file : bad format at the following line :\n$line\n");
		}
	}
	return @elements;
}

sub createObject {
	my ($fields, $objectType, $option_hash, $data_hash, $info) = @_;
	my $object;
	# Object type determination
	switch ($objectType) {
		case ("annotationFile") {
			$object = AnnotationFiles->new("temp_name");
		}
		case("printedField") {
			$object = PrintedFields->new();
		} else {
			die ("Programm error : unknown Configuration object.\n");
		}
	}
	# In function of the object, the setValue function will not be the same
	while (my ($key, $value) = each %{ $fields } ) {
		$object->setValue($key, $value, $option_hash, $data_hash); # Function which attributes the value to the key after testing both of them
	}

	# We control object validity and store it
	if ($object->isValid($data_hash)) {
		$object->store($data_hash);
	}
}


1;

