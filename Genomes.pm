package Genomes;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => "all";

use List::Util qw(min max);

# Will contain all chromosome information
our %authorized_chromosomes;
our @ordered_chromosomes;

# UTILISE
sub configureReference {
    print "## Configuration of the reference contigs ##\n";
    my ($option_hash) = @_;
    my $genome_fasta_index = $$option_hash{"files"}{"reference_contigs"}.".fai";
    my $format = "genome fasta index"; # Only for error messages
    if (-e $genome_fasta_index) {
        print "- Index file : ".$genome_fasta_index."\n";
        open(my $genome_fai_fh, "<", $genome_fasta_index) or die(Errors::formatFileOpeningError($format, $genome_fasta_index));
        my $chromosome_order = 0;
        foreach my $line (<$genome_fai_fh>) {
            $chromosome_order++;
            my @elements = Files::megaSplit($line);
            if(!(defined($elements[0])) || !(defined($elements[1]))) {
                die(Errors::badlyFormattedFile($genome_fasta_index, $format));
            }
            my $chromosome = $elements[0];
            push @ordered_chromosomes, $chromosome;
            $authorized_chromosomes{$chromosome}{"length"} = $elements[1];
            $authorized_chromosomes{$chromosome}{"order"} = $chromosome_order;
        }
    } else {
        die(Errors::inexistentFormatFile($genome_fasta_index, $format));
    }
    print "\n";
}

# UTILISE
sub dieIfNonexistentChromosome {
    my ($chromosome) = @_;
    if(!($authorized_chromosomes{$chromosome})){
        die Errors::unknownChromosome($chromosome);
    }
    return($chromosome);
}

sub getGenomicContext {
    my ($genome, $chromosome, $start, $end) = @_;
    my $context = `samtools faidx $genome $chromosome:$start-$end`;
    chomp($context);
    return($context);
}

sub prepareGenomicContextGetting {
    my ($chromosome, $start, $end, $genome, $distance_5p, $distance_3p) = @_;
    # Start determination - Cannot be inferior to 0
    $start = max(0, $start - $distance_5p);
    # End determination - Cannot be superior to chromosome length
    $end = min($end,$Genomes::authorized_chromosomes{$chromosome}{"length"});
    my $context = getGenomicContext($genome, $chromosome, $start, $end);
    return($context);
}

1;


