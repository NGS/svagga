package BED;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# A BED is an Interval with information fields, and putatively associated to a file
our @ISA = ("Intervals");

# Gene Object, an interval with a name, and other information such as transcripts
sub new {
	my ($class, $chromosome, $start, $end, $info, $file) = @_;
	$class = ref($class) || $class;
	my $bed = {};
	$bed = $class->Intervals::new( $chromosome, $start, $end );

	if (defined($info)) {
		$info =~ s/^\s*$//; # If the info field is composed only of spaces, it is considered as empty
		if ($info ne "") {
			$bed->{"info"} = $info;
		} else {
			$bed->{"info"} = "No info (".$bed->toSimpleIntervalString().")";
		}
	}
	$bed->{"file"} = $file;
	bless($bed, $class);
	return($bed);
}

# Function which create a BED object from a BED line
sub BEDobjectFromLine {
	my ($bed_line, $file) = @_;
	chomp($bed_line);
	$bed_line =~ s/\r//g;
	my @elements = split("\t", $bed_line);
	$elements[1]++; # A BED start has to be increased of 1
	# The end does has not to be taking into account, so no use to increase it
	my $bed = new BED($elements[0], $elements[1], $elements[2], join("/", @elements[3 .. $#elements]), $file);
	return($bed);
}

# Function which takes a separator and a hash, and return the hash values on a string of characters separated by the separator
sub aggregateHashInfo {
	my ($separator, $hash) = @_;
	my $string = "";
	my $at_least_one_info;
	while (my ($key, $value) = each %$hash) {
		if (defined($value->{"info"})) {
			$at_least_one_info = 1;
			$string .= $value->{"info"}.$separator." ";
		}
	}
	$string =~ s/$separator $//;
	if ($at_least_one_info) {
		return $string;
	} else {
		return "1";
	}
}

1;
