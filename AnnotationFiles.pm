package AnnotationFiles;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;

our %file_attributes = (
	"name" => 1,
	"format" => 1,
	# Shortcut is useful to call this specific file in other configuration files
	"shortcut" => 1,
	# Content
	"sorted" => 1,
	"content" => 1,
	"concatenated" => 1,
);

# An AnnotationFile is a file with specifities...
use Files;
our @ISA = ("Files");

sub new {
	my ($class, $name, $format) = @_;
	$class = ref($class) || $class;
	my $annotationFile = {};
	if (!(defined($format))) {
		$format = "BED"; # by default, the file is assumed to be at BED format
	}
	$annotationFile = $class->Files::new( $name, $format);
	$annotationFile->{"sorted"} = 0; # by default, the file is assumed unsorted
	$annotationFile->{"concatenated"} = 0; # by default, the file is assumed to contain overlaps
	$annotationFile->{"shortcut"} = undef;
	bless($annotationFile, $class);
	return($annotationFile);
}

sub setValue {
	my ($annotationFile, $key, $value) = @_;
	# Some error are tolerated
	switch (lc($key)) {
		case /file/ {
			$key = "name";
		}
		case /overl/ {
			$key = "concatenated";
		}
		case /sort/ {
			$key = "sorted";
		}
		case /track/ {
			$key = "shortcut";
		}
	}
	# ..some others are not
	if (!(defined($file_attributes{$key}))) {
		die("Unkown option in the annotation file configuration : $key\n");
	}
	$annotationFile->{$key} = $value;
}

				

sub forbiddenShortcut {
	my ($shortcut) = @_;
	switch (lc($shortcut)) {
		case /^genes?$/ {
			return(1);
		}
	}
}

# UTILISE
# Function which create annotation objects from annotation configuration files
sub configureAnnotationFiles {
	my ($option_hash, $data_hash) = @_;
	print "## Configuration of the annotation files ##\n";
	foreach my $annotation_file (@{ $$option_hash{files}{"annotation_file_configuration"} }) {
		print "# File : $annotation_file\n";
		my @annotationFiles = Configuration::getConfigurationFileElements($annotation_file);
		foreach my $annotationFile (@annotationFiles) {
			Configuration::createObject($annotationFile, "annotationFile", $option_hash, $data_hash);
		}
		die();
	}
	print "\n";
}

# Basic functions
sub isValid {
	my ($annotationFile, $data_hash) = @_;
	if (!(defined($annotationFile->{"name"}))) {
		die("Name is mandatory for an annotation file (--annotation_file_configuration) :\n".Objects::enumerate($annotationFile)."\n");
	} else {
		# The file has to exist
		if (!(-f $annotationFile->{"name"})) {
			die ("File non existing, or folder : ".$annotationFile->{"name"}."\n");
		}
		if (!(defined($annotationFile->{"shortcut"}))) {
			print "No shortcut for the file ".$annotationFile->{"name"}.".\nThis is not recommanded because it will be more difficult to use.\n".Errors::Documentation();
			$annotationFile->{"shortcut"} = $annotationFile->{"name"};
		}
		if (defined($$data_hash{"files"}{"annotations"}{ $annotationFile->{"shortcut"} })) {
			die ("File shortcut use twice (option --annotation_file_configuration) : ".$annotationFile->{"shortcut"}.".\n");
		}
		if (forbiddenShortcut($annotationFile->{"shortcut"})) {
			die ("Forbidden shortcut/Generic term with special meaning for Svagga (option --annotation_file_configuration) : ".$annotationFile->{"shortcut"}.".\n");
		}
	}
	return(1);
}

sub store {
	my ($annotationFile, $data_hash) = @_;
	$$data_hash{"files"}{"annotations"}{ $annotationFile->{"shortcut"} } = $annotationFile;
}


# Function which compare the current BED line with existing one to know if 
sub isSortedBED {
	my $annotation = $_[0];
	my $bed_interval = $_[1];

	if(defined($annotation->{"file_content"}{ $$bed_interval[0] }{"last_start"})) {
		if ( $annotation->{"file_content"}{ $$bed_interval[0] }{"last_start"} > $$bed_interval[1] ) {
			$annotation->notSortedBED($bed_interval);
		} 
		# TODO: check if BED end has to be sorted when start is the same
		#elsif ( $annotation->{"file_content"}{ $$bed_interval[0] }{"last_start"} == $$bed_interval[1] ) {
		#	if ( $annotation->{"file_content"}{ $$bed_interval[0] }{"last_end"} >  $$bed_interval[2] ) {
		#		$annotation->notSortedBED($bed_interval);
		#	}
		#}
	}
	$annotation->{"file_content"}{ $$bed_interval[0] }{"last_start"} = $$bed_interval[1];
	$annotation->{"file_content"}{ $$bed_interval[0] }{"last_end"} = $$bed_interval[2];
}


sub notSortedBED {
	my $annotation = $_[0];
	my $bed_interval = $_[1];
	my $error_message = "The BED file ".$annotation->{"file"}." is assumed to be sorted but is not :\n";
	$error_message .= $$bed_interval[0]."\t".$annotation->{"file_content"}{ $$bed_interval[0] }{"last_start"}."\t";
	$error_message .= $annotation->{"file_content"}{ $$bed_interval[0] }{"last_end"}."\n";
	$error_message .= $$bed_interval[0]."\t".$$bed_interval[1]."\t".$$bed_interval[2]."\n";
	print ($error_message);
}

# UTILISE
# Function which store and sorts all the annotation files of interest, if not already sorted
sub getAnnotationsFromFiles {
	my ($option_hash, $data_hash) = @_;
	print "## Acquisition of the Annotations ##\n";
	while (my ($shortcut, $annotationFile) = each %{ $$data_hash{"tracks_of_interest"}{"annotationFiles"} }) {
		print "# File : ".$annotationFile->{name}."\n";
		$annotationFile->getAnnotationsFromAFile($option_hash, $data_hash);
	}
	print "\n";
}

# UTILISE
sub getAnnotationsFromAFile {
	my ($annotationFile, $option_hash, $data_hash) = @_;
	# File opening
	$annotationFile->testFileOpening();
	# Content reading
	foreach my $line (readline($annotationFile->{file_handler})) {
		my $bed_instance = BED::BEDobjectFromLine($line, $annotationFile->{name});
		$bed_instance->storeAndSortOnChromosomeArray(\@{ $$data_hash{"annotations"}{"files"}{ $annotationFile->{name} }{"chromosomes"}{ $bed_instance->{"chromosome"} } });
	}
}

# UTILISE
# Function which annotates the breakpoints instance using iteratively the annotation objects 
sub annotateBreakPoints {
	my $option_hash = $_[0];
	my $data_hash = $_[1];
	print "## Annotation of the Breakpoints ##\n";
	# Determination of the array in which are stored the breakpoints that have to be annotated
	# TODO : better than that ! Breakpoints can be at the same time in events, calls, breakpoints...
	my $breakpoint_array = "calls";
	if ($$option_hash{breakpoints}{clustered}) {
		$breakpoint_array = "events";
	} elsif ($$option_hash{"task"} eq "breakpointAnnotation") {
		$breakpoint_array = "breakpoints";
	}
	# We only use the annotation tracks which are really printed
	while (my ($shortcut, $annotationFile) = each %{ $$data_hash{"tracks_of_interest"}{"annotationFiles"} }) {
		print "# Track : ".$annotationFile->{"shortcut"}."\n";
		SAMPLE: foreach my $sample (keys %{ $$data_hash{"samples"} }) {
			CHROMOSOME: foreach my $chromosome (@Genomes::ordered_chromosomes) {
				BREAKPOINT: foreach my $breakpoint (@{ $$data_hash{"samples"}{$sample}{$breakpoint_array}{"chromosomes"}{$chromosome} }) {
					ANNOTATION: foreach my $interval (@{ $$data_hash{"annotations"}{"files"}{ $annotationFile->{name} }{"chromosomes"}{$chromosome} }){
						my $distance = $breakpoint->distanceTo($interval);
						$breakpoint->updateTrackAnnotation($annotationFile->{"shortcut"}, $interval, $distance);
					}
				}
			}
		}
	}
	print "\n";
}























1;
